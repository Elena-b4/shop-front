
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from '../../axios-init';
import { pathCreater } from '../../helpers';
import { RootState } from '..';
import type { IFilters } from '../../types'

interface IFiltersState {
	filters: IFilters;
	error: any;
	loading: 'idle' | 'pending' | 'succeeded' | 'failed' | 'loading';
}

const initialState = {
	filters: {},
	loading: 'idle',
	error: null,
} as IFiltersState;

export const fetchCategoryFilters = createAsyncThunk(
'category-filters',
async (_, {getState}) => {
	const state = getState() as RootState;
	const response = await axios.get(pathCreater(`categories/${state.categoryName.value}`, state));
	return response.data;
});

const categoryFilters = createSlice({
	name: 'category-filters',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
	  builder
		.addCase(fetchCategoryFilters.pending, (state) => {
		  state.loading = 'loading';
		  state.error = null;
		})
		.addCase(fetchCategoryFilters.fulfilled, (state, action: PayloadAction<IFilters>) => {
		  state.filters = action.payload;
		  state.loading = 'idle';
		  state.error = null;
		})
		.addCase(fetchCategoryFilters.rejected, (state, action) => {
		  state.loading = 'failed';
		  state.error = action.error;
		});
	},
});

export default categoryFilters.reducer;


