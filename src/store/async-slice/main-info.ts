import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import axios from '../../axios-init'
import { pathCreater } from '../../helpers'
import { RootState } from '..'
import type { IMainInfo } from '../../types'

interface IMainInfoState {
	value: IMainInfo
	error: any
	loading: 'idle' | 'pending' | 'succeeded' | 'failed' | 'loading'
}

const initialState = {
	value: {},
	loading: 'idle',
	error: null,
} as IMainInfoState


export const fetchMainInfo = createAsyncThunk(
	'main-info',
	async (_, { getState }) => { 
		const state = getState() as RootState
		const response = await axios.get(pathCreater('main-info', state))
		return response.data
	})


const mainInfo = createSlice({
	name: 'main-info',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder
			.addCase(fetchMainInfo.pending, (state) => {
				state.loading = 'loading'
				state.error = null
			})
			.addCase(fetchMainInfo.fulfilled, (state, action: PayloadAction<IMainInfo>) => {
				state.value = action.payload
				state.loading = 'idle'
				state.error = null
			})
			.addCase(fetchMainInfo.rejected, (state, action) => {
				state.loading = 'failed'
				state.error = action.error
			})
	},
})

export default mainInfo.reducer