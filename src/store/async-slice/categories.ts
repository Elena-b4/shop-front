import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from '../../axios-init';
import { pathCreater } from '../../helpers';

interface ICategoryItem {
	icon: null | string[],
	slug: string;
	id: number;
	title: string;
	childdren: ICategoryItem[]
}
  
interface ICategoryState {
	categories: ICategoryItem[];
	error: any;
	loading: 'idle' | 'pending' | 'succeeded' | 'failed' | 'loading';
}

const initialState = {
	categories: [],
	loading: 'idle',
	error: null,
  } as ICategoryState


  export const fetchCategories = createAsyncThunk(
	'categories',
	async (_, {getState}) => {
	  const state = getState();
	  const response = await axios.get(pathCreater(`categories`, state));
	  return response.data;
	}
  );

  const categories = createSlice({
	name: 'categories',
	initialState,
	reducers: {},
	extraReducers: (builder) => {
	  builder
		.addCase(fetchCategories.pending, (state) => {
		  state.loading = 'loading';
		  state.error = null;
		})
		.addCase(fetchCategories.fulfilled, (state, action: PayloadAction<ICategoryItem[]>) => {
		  state.categories = action.payload;
		  state.loading = 'idle';
		  state.error = null;
		})
		.addCase(fetchCategories.rejected, (state, action) => {
		  state.loading = 'failed';
		  state.error = action.error;
		});
	},
  })


export default categories.reducer;
  