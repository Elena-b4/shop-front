
import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from '../../axios-init';
import { pathCreater } from '../../helpers';
import { RootState } from '..';
import type { IProductsData } from '../../types'
import { changeInitLoadState } from '../slice/init'
interface IProductsState {
	data: IProductsData | null;
	error: any;
	loading: 'idle' | 'pending' | 'succeeded' | 'failed' | 'loading';
}

const initialState = {
	data: null,
	loading: 'idle',
	error: null,
} as IProductsState;


export const fetchCategoryProducts = createAsyncThunk(
	'curret-products',
	async (_, { getState, dispatch }) => { 
		const state = getState() as RootState;
		try {
			if (state.categoryProducts.data === null) dispatch(changeInitLoadState(true))
			const response = await axios.get(pathCreater(`categories/${state.categoryName.value}/products/${state.categorySearchParams.params}`, state));
			if (state.init.value) dispatch(changeInitLoadState(false))
			return response.data;
		} catch (error) {
			if (state.init.value) {
				dispatch(changeInitLoadState(false))
				console.log(error)
				window.history.pushState(null, '', '/web-error') //
			}
		}
	});


const categoryProducts = createSlice({
	name: 'category-products',
	initialState,
	reducers: {
		sortCategory(state, action: PayloadAction<IProductsData>) {
			state.data = action.payload;
		}
	},
	extraReducers: (builder) => {
		builder
			.addCase(fetchCategoryProducts.pending, (state) => {
				state.loading = 'loading';
				state.error = null;
			})
			.addCase(fetchCategoryProducts.fulfilled, (state, action: PayloadAction<IProductsData>) => {
				state.data = action.payload;
				state.loading = 'idle';
				state.error = null;
			})
			.addCase(fetchCategoryProducts.rejected, (state, action) => {
				state.loading = 'failed';
				state.error = action.error;
			});
	},
});
export const { sortCategory  } = categoryProducts.actions;
export default categoryProducts.reducer;

// export const fetchCategorySortProducts = createAsyncThunk(
// 	'curret-category',
// 	async (_, { getState, dispatch }) => { 
// 		const state = getState() as RootState;

// 		//вынести в helper
		
// 		let query = ''
// 		if (state.sortType.type !== 'clean')  query = `order_type=${state.sortType.type}`
// 		query = query.length ? `?${query}` : query

// 		var baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
// 		var newUrl = baseUrl + query;
// 		window.history.pushState(null, '', newUrl);

// 		//вынести в helper

// 		const response = await axios.get(pathCreater(`categories/${state.categoryName.value}/products/${!!query ? query : ''}`, state));
// 		dispatch(changeLoadingStatus(false));
// 		dispatch(categoryProducts.actions.sortCategory(response.data))
// 		dispatch(changeLoadingStatus(true));
// 	});
