import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface ILoadAllCtegoryData {
    status: boolean;
}

const initialState = {
    status: false,
} as ILoadAllCtegoryData;

const loadCategoryData = createSlice({
    name: "load-category-data",
    initialState,
    reducers: {
        changeLoadingStatus(state, action: PayloadAction<boolean>) {
            state.status = action.payload;
        },
    },
});

export const { changeLoadingStatus } = loadCategoryData.actions;
export default loadCategoryData.reducer;
