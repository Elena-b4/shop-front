import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
interface ISortTypeState {
	params: string;
}

interface IPayloadType  {key: string, value: string | number | boolean}
const params = window.location.pathname.includes('catrgory') ? window.location.search : ''
const initialState = { params } as ISortTypeState;

const categorySearchParams = createSlice({
	name: 'category-search-params',
	initialState,
	reducers: {
		addParams(state, action: PayloadAction<IPayloadType>) {
			const searchParams = new URLSearchParams(state.params);
			if (!searchParams.has('order_type')) {
				searchParams.append('order_type', 'asc')
			}
			if(searchParams.has(action.payload.key)) {
				searchParams.delete(action.payload.key)
			}
			searchParams.append(action.payload.key, String(action.payload.value));
			const searchString = '?' + searchParams.toString();
			const newUrl = window.location.origin + window.location.pathname + searchString;
			console.log(newUrl)
			window.history.pushState(null, '', newUrl);
			state.params = searchString;
		},
		resetParams(state) {
			window.history.pushState(null, '', window.location.origin + window.location.pathname);
			state.params = '';
		},
	}
})

export const { addParams, resetParams } = categorySearchParams.actions
export default categorySearchParams.reducer

