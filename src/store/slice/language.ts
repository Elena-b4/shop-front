import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'

interface ILocalizationrState {
	value: string
  }
  
const initialState = { value:  'ru'} as ILocalizationrState

const language = createSlice({
	name: 'language',
	initialState,
	reducers: {
		changeLanguage(state, action: PayloadAction<string>) {
			state.value = action.payload;
		},
	}
})

export const { changeLanguage } = language.actions
export default language.reducer