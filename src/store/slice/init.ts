import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'
import { lockBody } from "../../helpers";

interface ILoadInit {
	value: boolean,
};


const initialState = { value: true } as ILoadInit;

const init = createSlice({
	name: 'init',
	initialState,
	reducers: {
		changeInitLoadState(state, action: PayloadAction<boolean>) {
			lockBody(action.payload)
			state.value = action.payload
		},
	}
});

export const { changeInitLoadState } = init.actions
export default init.reducer