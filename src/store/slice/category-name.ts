import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit'

interface IcategoryNameState {
	value: string
};

const initCategory = (): string => {
	const [path, category] = window.location.pathname.split('/').filter(p => p.length);
	return path === 'category' && category ? category : 'all';
};

const initialState = { value: initCategory() } as IcategoryNameState;

const categoryName = createSlice({
	name: 'category-name',
	initialState,
	reducers: {
		changeCategoryName(state, action: PayloadAction<string>,) {
			state.value = action.payload;
		},
	}
});

export const { changeCategoryName } = categoryName.actions
export default categoryName.reducer