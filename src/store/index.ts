import { configureStore } from '@reduxjs/toolkit'
import language from './slice/language'
import categories from './async-slice/categories'
import categoryName  from './slice/category-name'
import categoryProducts from './async-slice/category-products'
import categoryFilters from './async-slice/category-filters'
import loadCategoryData from './slice/load-category-data'
import categorySearchParams from './slice/category-search-params'
import mainInfo from './async-slice/main-info'
import { fetchCategories } from './async-slice/categories'
import { fetchMainInfo } from './async-slice/main-info'
import init from './slice/init'

export const store = configureStore({
  reducer: { init, language, categories, categoryName, categoryProducts, categoryFilters, loadCategoryData, categorySearchParams, mainInfo },
});


//первое и единственное получение навигации
store.dispatch(fetchCategories());
store.dispatch(fetchMainInfo());

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch