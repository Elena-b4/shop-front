
export const pathCreater = (absolutePath: string, state: any) => {
	const fixAbsolutPAth = absolutePath.charAt(0) === '/' ? absolutePath : `/${absolutePath}`
	const language = state?.language?.value || 'ru';
	return `/${language}/api/v1${fixAbsolutPAth}`
};

export const lockBody = (state: boolean) => document.body.classList[state ? 'add' : 'remove']('body-lock')
