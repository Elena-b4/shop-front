import './selected-arrow.scss';

const SelectedArrow = ({ isOpen }: { isOpen: Boolean }) => {
	const arrowClasses = isOpen ? 'selected-arrow selected-arrow__open' : 'selected-arrow';
	return (
		<svg width="12" height="8" viewBox="0 0 12 8" fill="none"
			className={arrowClasses}
			xmlns="http://www.w3.org/2000/svg">
			<path d="M5.46967 6.91302C5.76256 7.20591 6.23744 7.20591 6.53033 6.91302L11.3033 2.14005C11.5962 1.84716 11.5962 1.37228 11.3033 1.07939C11.0104 0.786497 10.5355 0.786497 10.2426 1.07939L6 5.32203L1.75736 1.07939C1.46447 0.786497 0.989593 0.786497 0.696699 1.07939C0.403806 1.37228 0.403806 1.84716 0.696699 2.14005L5.46967 6.91302ZM5.25 6V6.38269H6.75V6H5.25Z"
			/>
		</svg>
	)
}

export default SelectedArrow