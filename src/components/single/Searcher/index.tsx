import React from 'react'
import './searcher.scss'
//add debounce 3 sec
const Searcher = () => {
  return (
	<label className='searcher'>
		<input className='searcher-input' placeholder='Поиск' type="text" />
	</label>
  )
}

export default Searcher