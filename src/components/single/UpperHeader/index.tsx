import Logo from '../../layouts/Logo'
import LinkBasket from '../LinkBasket'
import LinkUser from '../LinkUser'
import Searcher from '../Searcher'
import MainInfo from '../MainInfo'

import './upper-header.scss'

const UpperHeader = () => {
  return (
	<div className='upper-header'>
		<Logo />
		<Searcher />
		<LinkBasket />
		<LinkUser />
		<MainInfo />
	</div>
  )
}

export default UpperHeader