import { Link } from 'react-router-dom';
import './link-user.scss';

const LinkUser = () => {
	return (
		<Link className="link-user" to='/user'>
			<svg className='link-user__vector' viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path className='link-user__path' d="M17.1216 6.75C16.9379 9.22828 15.0591 11.25 12.9966 11.25C10.9341 11.25 9.05209 9.22875 8.87162 6.75C8.68412 4.17188 10.5122 2.25 12.9966 2.25C15.481 2.25 17.3091 4.21875 17.1216 6.75Z" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
				<path className='link-user__path' d="M12.9972 14.25C8.91906 14.25 4.78 16.5 4.01406 20.7469C3.92172 21.2588 4.2114 21.75 4.74718 21.75H21.2472C21.7834 21.75 22.0731 21.2588 21.9808 20.7469C21.2144 16.5 17.0753 14.25 12.9972 14.25Z" strokeWidth="1.5" strokeMiterlimit="10" />
			</svg>
		</Link>
	)
}

export default LinkUser