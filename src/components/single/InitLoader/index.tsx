import logoSvg from '../../../assets/icon/logo.svg'
import './init-loader.scss'
const InitLoader = () => {
	return (
		<div className="init-loader">
			<img className="init-loader__image" src={logoSvg} alt="logo" />
		</div>
	)
}

export default InitLoader