import MainInfoPhone from '../MainInfoPhone';
import { useTypeSelector } from '../../../hooks'

import './main-info.scss'

const MainInfo = () => {
  const { phones } = useTypeSelector(state => state.mainInfo.value.companyInfo);
  return (
    <div className='main-info'>
      <ul className='main-info__phones'>
        {
          phones.length &&
          phones.map((phone, i) => <MainInfoPhone key={i} phone={phone} />)
        }
      </ul>
      <ul className='main-info__works'>
        <li className='main-info__work'>
          Пн-Пт 10:00-19:00
        </li>
      </ul>
    </div>
  )
}

export default MainInfo