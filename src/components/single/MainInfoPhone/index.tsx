import { FC } from 'react'
import phoneSvg from '../../../assets/icon/phone.svg'
import './main-info-phone.scss'

interface IProps {
	phone: string;
}

const MainInfoPhone: FC<IProps> = ({ phone }) => {
	const hrefTel = `tel: ${phone.split(' ').join('')}`
	return (
		<li className='main-info__phone'>
			<img className='phone-icon' src={phoneSvg} alt="phone-icon" />
			<a href={hrefTel} alt={phone} className='phone'>{phone}</a>
		</li>
	)

}
export default MainInfoPhone