import Container from '../Container'
import UpperHeader from '../../single/UpperHeader'
import Navigate from '../Navigate'
import './header.scss'

const Header = () => {
	return (
		<header className='header'>
			<Container>
				<UpperHeader />
				<Navigate />
			</Container>
		</header>
	)
}

export default Header

