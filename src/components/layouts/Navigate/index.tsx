import { NavLink } from 'react-router-dom'
import { useTypeSelector, useLanguage } from '../../../hooks'

import './navigate.scss'

const Navigate = () => {
	const { categories } = useTypeSelector(state => state.categories)
	const language = useLanguage()

	const linkClasses = ({ isActive }: { isActive: boolean }): string => `nav-link${isActive ? ' nav-link__active' : ''}`

	const createLink = (slug: string): string => `${language}/category/${slug}`

	return (
		<nav className='navigate'>
			{
				categories.length &&
				categories.map(({ slug, title, id }) => <NavLink className={linkClasses} to={createLink(slug)} key={id}>{title}</NavLink>)
			}
		</nav>
	)
}

export default Navigate