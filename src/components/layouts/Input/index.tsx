import { FC, ChangeEvent } from 'react'
import './input.scss'

type TInputProps = {
	value: string | number;
	type?: 'text' | 'email' | 'password';
	onChange?: (event: ChangeEvent<HTMLInputElement>) => unknown;
	onInput?: (event: any) => unknown;
}

const Input: FC<TInputProps> = ({ value, type, onChange, onInput }) => {
	const inputType = type ?? 'text'
	return (
		<label className='input-wrapper'>
			<input type={inputType} value={value} className='input' onChange={onChange} onInput={onInput} />
		</label>
	)
}

export default Input