import { ReactNode } from 'react'
import { CSSTransition } from 'react-transition-group'
import './transition-in-out.scss'

interface IProps {
	show: boolean
	children: ReactNode
}

const TransitionInOut = ({ show, children }: IProps) => {
  return (
	<CSSTransition in={show} timeout={500} classNames="transition-in-out" unmountOnExit>
		{children}
	</CSSTransition>
  )
}

export default TransitionInOut