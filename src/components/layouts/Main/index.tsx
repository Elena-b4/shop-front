import { Outlet } from 'react-router-dom';
import Container from '../Container';

import './main.scss';

const Main = () => {
	return (
		<main className='main'>
			<Container>
				<Outlet />
			</Container>
		</main>
	)
}

export default Main