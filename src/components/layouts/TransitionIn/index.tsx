import { ReactNode } from 'react'
import { CSSTransition } from 'react-transition-group'
import './transition-in.scss'

interface IProps {
	show: boolean;
	children: ReactNode;
}

const TransitionIn = ({ show, children }: IProps) => {
  return (
	<CSSTransition in={show} timeout={700} classNames="transition-out" unmountOnExit>
		{children}
	</CSSTransition>
  )
}

export default TransitionIn