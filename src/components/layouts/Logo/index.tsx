import React from 'react'
import { Link } from 'react-router-dom'
import './logo.scss'
import logoSvg from '../../../assets/icon/logo.svg'
import { useTypeSelector, useLanguage } from '../../../hooks'

const Logo = () => {
	const mainInfo = useTypeSelector(state => state.mainInfo)
	const name = mainInfo?.value?.companyInfo?.name || ''
	const to = useLanguage()
	return (
		<Link className='logo' to={to} title='Главная'>
			<img src={logoSvg} alt="logo" />
			<span className="logo-title">{name}</span>
		</Link>
	)
}

export default Logo