import { ReactNode } from 'react'
import { CSSTransition } from 'react-transition-group'
import './transition-out.scss'

interface IProps {
	show: boolean
	children: ReactNode
}

const TransitionOut = ({ show, children }: IProps) => {
  return (
	<CSSTransition in={show} timeout={500} classNames="transition-out" unmountOnExit>
		{children}
	</CSSTransition>
  )
}

export default TransitionOut