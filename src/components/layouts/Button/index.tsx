import { FC, ReactNode } from 'react'
import './button.scss'
type ButtonProps = {
	children?: ReactNode;
	round?: boolean;
	black?: boolean;
	outline?: boolean;
	onClick: (event: Event) => void;
}
const Button: FC<ButtonProps> = ({ children, round, black, outline, onClick }) => {
	const btnClasse = `btn ${round ? 'btn-round' : ''} ${black ? 'btn-black' : ''} ${outline ? 'btn-outline' : ''}`
	return (
		<button className={btnClasse} onClick={onClick}>
			{children}
		</button>
	)
}

export default Button