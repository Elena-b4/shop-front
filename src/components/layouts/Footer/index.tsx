import Container from '../Container';
import './footer.scss';

const Footer = () => {
	return (
		<Container>
			<footer className='footer'>a-shop 2022 </footer>
		</Container>
	)
}

export default Footer