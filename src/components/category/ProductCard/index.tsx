import { FC, useState } from 'react'
import ProductCardImages from './../ProductCardImages/index'
import { Link } from 'react-router-dom'
import ProductCardContol from '../ProductCardContol'
import TransitionInOut from '../../layouts/TransitionInOut'

import type { IProduct } from '../../../types'

import './product-card.scss'



const ProductCard: FC<IProduct> = ({ id, short_description, price, slug, hit, is_new, uuid, name, brand, main_image, images }) => {

  const sliceImages = images.slice(0, 3)
  const [showControl, setShowControl] = useState(false)

  const onMouseEnter = () => setShowControl(true)
  const onMouseLeave = () => setShowControl(false)

  return (
    <Link to={`product/${slug}`}>
      <div className='product-card' onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>
        {sliceImages.length && <ProductCardImages images={sliceImages} />}
        <p className='product-card__price'>{price} BYN</p>
        <h4 className='product-card__name'>{name}</h4>
        <p className='product-card__description'>{short_description}</p>
        <TransitionInOut show={showControl}>
          <ProductCardContol />
        </TransitionInOut>
      </div>
    </Link>
  )
}

export default ProductCard
