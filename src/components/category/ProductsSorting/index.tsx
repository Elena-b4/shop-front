import { useRef, useState, useEffect } from 'react';
import SelectedArrow from '../../icon/SelectedArrow';
import { useGetSearchParams, useTypeDispatch } from '../../../hooks';
import { addParams } from '../../../store/slice/category-search-params';
import type { TSortingTypes } from '../../../types';

import './product-sorting.scss';

type TSort = { [key: string]: string };

const sorting: TSort = {
  clean: 'Без сортировки',
  new: 'Новые',
  popular: 'Популярные',
  price: 'По цене'
}

const ProductsSorting = () => {
  const selectRef = useRef<HTMLDivElement>(null);
  const type = useGetSearchParams('sort', 'clean');
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const dispatch = useTypeDispatch();

  const listenClickEvent = (event: Event) => {
    const target = event.target as HTMLElement;
    if (target.closest('.product-sorting') !== selectRef.current) {
      setIsOpen(false);
    }
  };

  const listenScrollEvent = () => {
    const blockPosition = selectRef.current?.offsetTop || 0;
    const scrollPosition = window.scrollY;
    if (blockPosition + 100 < scrollPosition) setIsOpen(false);
  };

  const optionListClasses = isOpen ? 'options-list options-list__show' : 'options-list';
  const optionItemClasses = (key: string): string => `option-item ${key === type ? 'option-item__selected': ''}`
  const toggleOptionsHandler = () => setIsOpen(!isOpen);

  const selectHandler = (key: string) => {
    const temp = key as TSortingTypes;
    dispatch(addParams({ key: 'sort', value: temp }))
    setIsOpen(false);
  };

  useEffect(() => {
    window.addEventListener('click', listenClickEvent);
    window.addEventListener('scroll', listenScrollEvent);
    return () => {
      window.removeEventListener('click', listenClickEvent);
      window.removeEventListener('scroll', listenScrollEvent);
    }
  }, [isOpen]);

  return (
    <div ref={selectRef} className='product-sorting'>
      <button className='selected' onClick={toggleOptionsHandler}>
        {sorting[type]}
        <SelectedArrow isOpen={isOpen} />
      </button>
      <ul className={optionListClasses}>
        {Object.keys(sorting).map((key) => <li className={optionItemClasses(key)} key={key} onClick={() => selectHandler(key)}>{sorting[key]}</li>)}
      </ul>
    </div>
  )
}

export default ProductsSorting