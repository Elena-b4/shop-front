import { useState } from 'react'
import TransitionInOut from '../../layouts/TransitionInOut'
import './product-card-images.scss'

interface Images {
	name: string
}
interface iProps {
	images: Images[]
}
const ProductCardImages = ({ images }: iProps) => {
	const [activeIndex, setActiveIndex] = useState(0)
	const activeImage = images[activeIndex].name
	const [showPagination, setShowPagination] = useState(false)


	const coords = (event: MouseEvent) => {
		if (images.length === 1) return
		const block = event.currentTarget as HTMLElement
		const clientX = event.clientX
		const { left, width } = block.getBoundingClientRect()
		const x = clientX - left

		const screenPercent = x / (width / 100)

		if (images.length === 2) setActiveIndex(screenPercent <= 50 ? 0 : 1)
		else {
			let index = 0
			if (screenPercent < 33) index = 0
			else if (screenPercent < 66) index = 1
			else index = 2
			setActiveIndex(index)
		}
	}

	const onMauseEnter = (event: MouseEvent) => {
		coords(event)
		setShowPagination(true)
	}
	const onMauseMove = (event: MouseEvent) => coords(event)
	const onMauseLeave = () => setShowPagination(false)


	const styleBullet = { flexBasis: `${100 / images.length}%` }
	const isActiveBullet = (index: number): string => `pagination-bullet${index === activeIndex ? ' pagination-bullet__active' : ''}`
	return (
		<div className="product-card__images" onMouseEnter={onMauseEnter} onMouseMove={onMauseMove} onMouseLeave={onMauseLeave}>
			<img className="image" key={activeIndex} src={activeImage} alt="img" />
			<TransitionInOut show={showPagination}>
				<div className='pagination'>
					{
						images.length &&
						images.map(({name}, index) => <span className={isActiveBullet(index)} style={styleBullet} key={name} />)
					}
				</div>
			</TransitionInOut>
		</div>
	)
}

export default ProductCardImages