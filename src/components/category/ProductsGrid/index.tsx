
import { useTypeSelector } from '../../../hooks'
import ProductCard from '../ProductCard'
import ProductCardSkeleton from '../ProductCardSkeleton'
//import type { IProduct } from '../../../store/async-slice/category-products'

import './products-grid.scss'

const PropductsGrid = () => {
  const { categoryProducts, loadCategoryData: { status } } = useTypeSelector(state => state)
  const products = categoryProducts?.data?.products?.data || []
  const productsGridClasses = products.length ? 'products-grid' : 'products-grid products-grid__block'
  return (
    <div className={/*roductsGridClasses*/ 'products-grid'}>
      {!status ?
        Array.from({ length: 6 }).map((_, index) => <ProductCardSkeleton key={index} />)
        : products.length ?
          products.map(product => <ProductCard {...product} key={product.id} />) :
          <p>В данной категория пока нет товаров!</p>
      }
    </div>
  )
}

export default PropductsGrid