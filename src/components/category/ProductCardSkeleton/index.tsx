import React from "react"
import ContentLoader from "react-content-loader"
const ProductCardSkeleton = () => (
	<ContentLoader
		speed={2}
		width={288}
		height={520}
		viewBox="0 0 288 520"
		backgroundColor="#D5D0D0"
		foregroundColor="#F8F3F1"
		title="Загрузка..."
	>
		<rect x="366" y="256" rx="0" ry="0" width="0" height="1" />
		<rect x="15" y="145" rx="0" ry="0" width="50" height="0" />
		<rect x="73" y="99" rx="0" ry="0" width="34" height="0" />
		<rect x="0" y="6" rx="10" ry="10" width="288" height="320" />
		<rect x="10" y="414" rx="10" ry="10" width="268" height="40" />
		<rect x="10" y="340" rx="10" ry="10" width="195" height="25" />
		<rect x="10" y="378" rx="10" ry="10" width="268" height="20" />
	</ContentLoader>
);


export default ProductCardSkeleton