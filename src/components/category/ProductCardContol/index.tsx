import Favorites from '../../icon/Favorites'
import Button from '../../layouts/Button'

import './product-card-control.scss'

const ProductCardContol = () => {

	const addBasket = (event: Event) => {
		event.preventDefault()
		console.log('add')
	}

	return (
		<div className='product-card__control'>
			<Button round onClick={addBasket}>В корзину</Button>
			<Favorites />
		</div>
	)
}

export default ProductCardContol