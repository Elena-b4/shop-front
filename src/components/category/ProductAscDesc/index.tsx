import { addParams } from '../../../store/slice/category-search-params'
import { useGetSearchParams, useTypeDispatch } from '../../../hooks'
import sortArrow from '../../../assets/icon/sort-arrow.svg'
import './product-asc-desc.scss'

const ProductAscDesc = () => {
  const type = useGetSearchParams('order_type', 'asc')
  const dispatch = useTypeDispatch()
  const classesArrow = `order-type order-type__${type}`
  const changeType = () => {
    console.log('type: ', type, type === 'asc' ? 'desc' : 'asc')
    dispatch(addParams({ key: 'order_type', value: type === 'asc' ? 'desc' : 'asc' }))
  }
  return (
    <button className='order-type__toggler' onClick={changeType}>
      <img className={classesArrow} src={sortArrow} alt="sort" />
    </button>
  )
}

export default ProductAscDesc