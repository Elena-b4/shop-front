import { useTypeSelector } from '../../../hooks';
import ProductsSorting from '../ProductsSorting';
import ProductAscDesc from '../ProductAscDesc';
import './category-head.scss';


const CategoryHead = () => {
  const { loadCategoryData: { status }, categoryProducts: { data } } = useTypeSelector(state => state);
  const totalCategoryProducts = data?.products?.total || 0
  return (
    <div className='category-head'>
      <div className="head-info">
        <h1 className='head-title'>Каталог</h1>
        {status && <span className='head-products'>{totalCategoryProducts} товаров</span>}
      </div>
      <div className='head-sorting'>
        <ProductAscDesc />
        <ProductsSorting />
      </div>

    </div>
  )
}

export default CategoryHead