import React from 'react'
import { Link } from 'react-router-dom'
import { useLanguage } from '../../hooks/index';

const Error = () => {
	const language = useLanguage();
	const to = `/${language}`
	return (
		<div>
			<h1>404</h1>
			<Link to={to}> go home</Link>
		</div>
	)
}

export default Error