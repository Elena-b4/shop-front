import { useEffect } from "react";
import { useParams } from "react-router-dom"
import { useTypeDispatch } from '../../hooks/index';
import { changeCategoryName } from "../../store/slice/category-name";
import { useTypeSelector } from "../../hooks/index";
import CategoryHead from "../../components/category/CategoryHead";
import CategoryFilters from "../../components/category/CategoryFilters";
import ProductsGrid from "../../components/category/ProductsGrid";

import './category.scss';

const Category = () => {
	const { categorySlug } = useParams();
	const dispatch = useTypeDispatch();
	const { loadCategoryData: { status } } = useTypeSelector(state => state);

	const categoryClasses = `category-grid ${!status ? ' category-grid__load' : ''}`

	useEffect(() => {
		if (categorySlug) {
			dispatch(changeCategoryName(categorySlug));
			//dispatch(resetParams()) //перенести в extra reset change category
		}

	}, [categorySlug, dispatch]);

	return (
		<section className='category'>
			<CategoryHead />
			<div className={/*categoryClasses*/  'category-grid'}>
				<CategoryFilters />
				<ProductsGrid />
			</div>
		</section>
	)
}

export default Category;