import { useTypeSelector, useUpdateCategory } from './hooks';
import './App.scss';
import Router from './Router';
import InitLoader from './components/single/InitLoader'
import TransitionOut from './components/layouts/TransitionOut'
import TransitionIn from './components/layouts/TransitionIn';

const App = () => {
  const init = useTypeSelector(state => state.init.value)
  useUpdateCategory();
  return (
    <div className="app">
      <TransitionOut show={init}>
        <InitLoader />
      </TransitionOut>
      {
        !init &&
        <TransitionIn show={!init}>
          <Router />
        </TransitionIn>
      }
    </div>
  );
}

export default App;
//TODO order_type ask, desk