import React from 'react';
import ReactDOM from 'react-dom/client';
import './scss/_index.scss';
import App from './App';
import { store } from './store';
import { Provider } from 'react-redux';


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLDivElement
);
root.render(
  <React.StrictMode>
    <Provider store={store} children={<App />} />
  </React.StrictMode>
);

