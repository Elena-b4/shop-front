import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import type { TypedUseSelectorHook } from "react-redux";
import type { RootState, AppDispatch } from "../store";


import { changeLoadingStatus } from '../store/slice/load-category-data'
import { fetchCategoryProducts } from '../store/async-slice/category-products';
import { fetchCategoryFilters } from '../store/async-slice/category-filters';

export const useTypeDispatch: () => AppDispatch = useDispatch
export const useTypeSelector: TypedUseSelectorHook<RootState> = useSelector;

export const useUpdateCategory = () => {
	const { categoryName, categorySearchParams: { params } } = useTypeSelector(state => state);
	const dispatch = useTypeDispatch();
	useEffect(() => {
		const loadAllCategoryData = async (): Promise<void> => {
			dispatch(changeLoadingStatus(false));
			await Promise.all([
				dispatch(fetchCategoryProducts()),
				dispatch(fetchCategoryFilters()),
			])
			dispatch(changeLoadingStatus(true));
		}
		loadAllCategoryData()
	}, [dispatch, categoryName, params])
}

export const useGetSearchParams = (key: string, defaultValue: string) => {
	const { params } = useTypeSelector(state => state.categorySearchParams);
	const { search } = window.location;
	const searchString = params.length ? params : search.length ? search : '';
	const searchParams = new URLSearchParams(searchString);
	return searchParams.get(key) || defaultValue;
  }

export const useLanguage = (): string => {
	const language = useTypeSelector(state => state.language.value);
	if (language === 'ru') return ''
	else return `/${language}`
} 