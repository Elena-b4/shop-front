export type TSortingTypes = 'clean' | 'new' | 'popular' | 'price'






interface iFiltersDataValues {
	id: number,
	title: string;
}

interface IFiltersTypes {
	id: number;
	key: string;
	title: string;
	values: iFiltersDataValues[];
}
interface ICategory {
	id: number;
	slug: string;
	icon?: any;
	title: string;
	filters: IFiltersTypes[];
}

interface IValues {
	[key: string]: number
}

interface IPrice {
	minPrice: string;
	maxPrice: string;
}

interface IValue {
	id: number;
	title: string;
}

interface IBrands {
	key: string;
	title: string;
	values: IValue[];
}

export interface IFilters {
	category: ICategory;
	values: IValues;
	price: IPrice;
	brands: IBrands;
}

interface IProductImage {
	name: string;
}
interface IProductLink {
	url: string;
	label: string;
	active: boolean;
}
export interface IProduct {
	id: number;
	short_description: string;
	price: string;
	slug: string;
	hit: boolean;
	is_new: boolean;
	uuid: string;
	name: string;
	brand: string;
	main_image: string;
	images: IProductImage[];
}

interface IProductsValues {
	[key: string]: number;
}

interface IProducts {
	current_page: number;
	data: IProduct[],
	first_page_url: string;
	from: number;
	last_page: number;
	last_page_url: string;
	links: IProductLink[];
	next_page_url: null | string;
	path: string;
	per_page: number;
	prev_page_url: null | string;
	to: number;
	total: number;
}

export interface IProductsData {
	products: IProducts;
	values: IProductsValues;
}




 export interface ICompanyInfo {
	phones: string[];
	emails: string;
	description: string;
	address: string;
	name: string;
	meta_description: string;
	meta_tag: string;
}

export interface IMainInfo {
	socials: any[];
	companyInfo: ICompanyInfo;
}


