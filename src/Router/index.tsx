import { BrowserRouter, Routes, Route } from "react-router-dom"
import { useLanguage } from "../hooks"
import Layout from '../components/layouts/Layout'
import Home from '../pages/Home'
import Category from '../pages/Category'
import Product from '../pages/Product'
import Basket from '../pages/Basket'
import User from "../pages/User"
import Error from '../pages/Error'
import WebError from '../pages/WebError'

const Router = () => {
	const language = useLanguage()
	return (
		<BrowserRouter>
			<Routes>
				<Route path={`${language}/`} element={<Layout />}>
					<Route index element={<Home />} />
					<Route path={`${language}/category/:categorySlug`} element={<Category />} />
					<Route path='category/:category/product/:productSlug' element={<Product />} />
					<Route path='basket' element={<Basket />} />
					<Route path='user' element={<User />} />
					<Route path='*' element={<Error />} />
				</Route>
				<Route path='/web-error' element={<WebError />} />
			</Routes>
		</BrowserRouter>
	)
}

export default Router